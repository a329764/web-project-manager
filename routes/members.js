var express = require('express');

var router = express.Router();

const controller = require('../controllers/members');

router.get('/', controller.enlistMembers)

router.get('/:id', controller.findMember)

router.patch('/:id', controller.updateMember)

router.delete('/:id', controller.deleteMember)

router.post('/', controller.newMember)

module.exports = router;