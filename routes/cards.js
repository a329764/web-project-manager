const express = require('express');
const router = express.Router();

const controller = require('../controllers/cards');

/* GET users listing. */

router.post('/', controller.create);

router.get('/', controller.enlist);

router.get('/:id', controller.find);

router.patch('/:id', controller.update);

router.delete('/:id', controller.deleteCard);

module.exports = router;
