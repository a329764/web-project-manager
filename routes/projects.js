var express = require('express');

var router = express.Router();

const controller = require('../controllers/projects');

router.post('/', controller.createProject)

router.get('/', controller.enlistProjects)

router.get('/:id', controller.findProject)

router.patch('/:id', controller.updateProject)

router.delete('/:id', controller.deleteProject)

module.exports = router;
