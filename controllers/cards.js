var express = require('express');

const app = express()

app.use(express.static("public"))

let cards= [10];
class Card {
	constructor(id, priority, size, name, role, funcionality, benefit, context, columndId) {
		this.id = id;
		this.priority = priority;
        this.size = size;
        this.name = name;
        this.role = role;
        this.funcionality = funcionality;
        this.benefit = benefit;
        this.context = context;
        this.columndId = columndId;
	}

	updateInformation(priority, size, name, role, funcionality, benefit, context, columndId) {
		this.priority = priority;
        this.size = size;
        this.name = name;
        this.role = role;
        this.funcionality = funcionality;
        this.benefit = benefit;
        this.context = context;
        this.columndId = columndId;
	}

	
}

var indx=0;


function create(req, res, next) {
	//validate receives all parameters from body-forms and create new card
	if(req.body.priority!= null & req.body.size!= null & req.body.name!= null & req.body.role!= null & req.body.funcionality!= null & req.body.benefit!= null & req.body.context != null & req.body.columndId != null){
		cards[indx] = new Card(indx, req.body.priority, req.body.size, req.body.name, req.body.role, req.body.funcionality, req.body.benefit, req.body.context, req.body.columndId);
		indx++;
	}

	//res.send(cards[indx-1]);
	res.send("Creado");
}

function enlist(req, res, next) {
	res.send(cards);
}

function find(req, res, next) {
	//res.send(cards[req.params.id]);
	res.send("Encontrado");
}

function update(req, res, next) {
	//validate receives all parameters from body-forms and UPDATE the index card
	if(req.body.priority!= null & req.body.size!= null & req.body.name!= null & req.body.role!= null & req.body.funcionality!= null & req.body.benefit!= null & req.body.context != null & req.body.columndId != null){
		cards[req.params.id].updateInformation(req.body.priority, req.body.size, req.body.name, req.body.role, req.body.funcionality,req.body.benefit, req.body.context,req.body.columndId);
	}
	res.send("Actualizado");
}

function deleteCard(req, res, next) {
	//cards.remove(req.params.id);
	res.send("Eliminado");
}



module.exports = {
	create, enlist, find, update, deleteCard
  }
