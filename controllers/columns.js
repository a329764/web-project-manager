var express = require('express');

const app = express()

app.use(express.static("public"))

let columns= [10];
class Column {
	constructor(id, name, projectId) {
		this.id = id;
		this.name = name;
    this.projectId = projectId;
	}

	updateInformation(name, projectId) {
		this.name = name;
    this.projectId = projectId;
	}

	
}

var indx=0;


function create(req, res, next) {
	//validate receives all parameters from body-forms and create new column
	if(req.body.priority!= null & req.body.size!= null & req.body.name!= null & req.body.role!= null & req.body.funcionality!= null & req.body.benefit!= null & req.body.context != null & req.body.columndId != null){
		columns[indx] = new Column(indx, req.body.priority, req.body.size, req.body.name, req.body.role, req.body.funcionality, req.body.benefit, req.body.context, req.body.columndId);
		indx++;
	}

	//res.send(columns[indx-1]);
	res.send("Creado");
}

function enlist(req, res, next) {
	res.send(columns);
}

function find(req, res, next) {
	//res.send(columns[req.params.id]);
	res.send("Encontrado");
}

function update(req, res, next) {
	//validate receives all parameters from body-forms and UPDATE the index column
	if(req.body.priority!= null & req.body.size!= null & req.body.name!= null & req.body.role!= null & req.body.funcionality!= null & req.body.benefit!= null & req.body.context != null & req.body.columndId != null){
		columns[req.params.id].updateInformation(req.body.priority, req.body.size, req.body.name, req.body.role, req.body.funcionality,req.body.benefit, req.body.context,req.body.columndId);
	}
	res.send("Actualizado");
}

function deleteColumn(req, res, next) {
	//columns.remove(req.params.id);
	res.send("Eliminado");
}



module.exports = {
	create, enlist, find, update, deleteColumn
  }
