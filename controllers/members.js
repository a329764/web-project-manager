var express = require('express');

const app = express()

app.use(express.static("public"))

const path = require('path')

class Member {
	constructor(name, email) {
		this.name = name;
		this.email = email;
	}

	setPassword() {
		this.password = 'Get from user.';
		this.id = 'Created when password is received.';
	}

	getInformation() {
		this.dob = 'Get from user.';
		this.curp = 'Get from user.';
		this.rfc = 'Get from user.';
		this.address = 'Get from user.';
	}

	// skillList[] to be implemented;
}

function enlistMembers(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/members/enlist.html'));
}

function findMember(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/members/find.html'));
}

function updateMember(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/members/update.html'));
}

function deleteMember(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/members/delete.html'));
}

function newMember(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/members/signup.html'));
}
module.exports = {
	enlistMembers, findMember, updateMember, deleteMember, newMember
  }
// signIn to be implemented;
