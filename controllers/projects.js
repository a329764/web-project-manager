var express = require('express');

const app = express()

app.use(express.static("public"))

const path = require('path')

class Project {
	constructor(name, description) {
		this.name = name;
		this.description = description;
	}

	generateId() {
		this.id = 'Created when project is created.';
	}

	setDates() {
		this.deliveryDate = 'Scheduled by system.';
		this.startingDate = 'Scheduled by system.';
		this.rfc = 'Get from user.';
		this.address = 'Get from user.';
	}

	getTeamInfo() {
		this.managerId = 'Get from user.';
		this.ownerId = 'Get from user.';

		// teamMembers[] to be implemented;
	}
}

function createProject(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/projects/create.html'));
}

function enlistProjects(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/projects/enlist.html'));
}

function findProject(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/projects/find.html'));
}

function updateProject(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/projects/update.html'));
}

function deleteProject(req, res, next) {
	res.sendFile(path.join(__dirname, '/../public/projects/delete.html'));
}
module.exports = {
	createProject, enlistProjects, findProject, updateProject, deleteProject
  }
