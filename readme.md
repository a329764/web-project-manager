<a href="https://gitlab.com/a329764/web-project-manager/-/tree/final">Proyecto completo en la rama "final"</a>
<h1>Integrantes</h1>
<h3>329764 — Gabriel Mar Barrio</h3>
<h3>329610 — Luis Fernando Félix Mata</h3>
<h3>329748 — Julián Terán Vázquez</h3>
<h3>329694 — José Daniel Hermosillo López</h3>



# Model Diagram
![N|Solid](https://i.imgur.com/DwnyKyj.png)\

# Sequence Diagram
![N|Solid](https://i.imgur.com/uFNSXMp.png)
